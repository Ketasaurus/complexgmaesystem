#pragma once
#define GLM_SWIZZLE
#include <gl_core_4_4.h>
#include <glfw\include\GLFW\glfw3.h>
#include <cstdio>
#include "Gizmos.h"
#include <glm\glm\glm.hpp>
#include <glm\glm\ext.hpp>
#include <iostream>
#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

class Application
{
public:

	Application();

	Application(int _width, int _height, const char* name, GLFWmonitor * _monitor = nullptr, GLFWwindow* _share = nullptr);

	virtual ~Application();

	void Run();

	virtual void Update(float deltaTime);

	virtual void Render();

	virtual void Startup();

	virtual int Shutdown();

	void Initialise();

	int initGLFW(int width, int height, const char* name);

	int initGL();

	void setupProjection(mat4 m_projection);

	void setupView(mat4 m_view);

	void shutdownGLFW();

	float GetdeltaTime();







protected:

	GLFWwindow* m_window;

	mat4 m_projection;
	mat4 m_view;

	float m_previousTime;
	float m_deltaTime;

	unsigned int m_uiScreenWidth;
	unsigned int m_uiScreenHeight;

	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_IBO;

	unsigned int m_programID;
};
