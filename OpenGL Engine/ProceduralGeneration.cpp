#include "ProceduralGeneration.h"



ProceduralGeneration::ProceduralGeneration(int _width, int _height, const char* name) : Application(_width, _height, name)
{
	shader = Shader("./Shaders/Vertex/default.vert", "./Shaders/Fragment/default.frag");
	shader.Use();
	int i = glGetUniformLocation(shader.Program, "snow");
	glUniform1i(i, 0);
	i = glGetUniformLocation(shader.Program, "water");
	glUniform1i(i, 1);

	generateGrid(100, 100);

}

ProceduralGeneration::ProceduralGeneration()
{
	shader = Shader("./Shaders/Vertex/default.vert", "./Shaders/Fragment/default.frag");
	shader.Use();
	int i = glGetUniformLocation(shader.Program, "snow");
	glUniform1i(i, 0);
	i = glGetUniformLocation(shader.Program, "water");
	glUniform1i(i, 1);

	generateGrid(100, 100);
}


ProceduralGeneration::~ProceduralGeneration()
{
}

void ProceduralGeneration::Startup()
{
	
}

void ProceduralGeneration::Update(float deltaTime)
{


}

void ProceduralGeneration::generateGrid(unsigned int rows, unsigned int cols)
{
	//generates new vertex using rows and cols as viriables
	Vertex* aoVertices = new Vertex[rows * cols];

	float* perlinData = new float[rows * cols];

	float scale = (1.0f / rows) * 3;

	unsigned int* auiIndices = new  unsigned int[(rows - 1)* (cols - 1) * 6];

	for (unsigned int r = 0; r < rows; ++r) {

		for (unsigned int c = 0; c < cols; ++c) {

			// defining index count based off quad count (2 triangles per quad)

			float y = (glm::perlin(vec2(c, r) * scale * frequency) * 0.5f + 0.5f) * amplitude;
			perlinData[r * cols + c] = y;
			aoVertices[r * cols + c].position = vec4((float)c, y, (float)r, 1);
			aoVertices[r * cols + c].colour = vec4(y / (rows - 1), y / (rows - 1), y / (rows - 1), 1);
			aoVertices[r * cols + c].texCoord = vec2(r, c);

		}
	}
	m_rows = rows;
	m_cols = cols;

	unsigned int index = 0;
	for (unsigned int r = 0; r < (rows - 1); ++r) {

		for (unsigned int c = 0; c < (cols - 1); ++c) {

			// triangle 1
			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			// triangle 2
			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			auiIndices[index++] = r * cols + (c + 1);
			//The above code segment creates an index array, as we are drawing our grid as a series of quads we have generated the index positions for each triangle in this grid.
		}
	}

	// create and bind buffers to a vertex array object
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_IBO);

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);

	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(Vertex), aoVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4)));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4) * 2));

	int imageWidth = 0;
	int imageHeight = 0;
	int imageFormat = 0;
	unsigned char* data = stbi_load("./Textures/snow.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);
	glGenTextures(1, &m_perlin_texture);
	glGenTextures(1, &m_perlin_texture2);
	glGenTextures(1, &m_perlin_texture3);


	//texture 1 - snow
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_LINEAR);

	//texture 2 - water
	unsigned char* data1 = stbi_load("./Textures/water.jpg", &imageWidth, &imageHeight, &imageFormat, STBI_default);
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture2);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data1);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_LINEAR);

	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//This code segment will create and bind our VBO and fill it with the vertex data for our grid. 
	// glEnableVertexAttribArray and glVertexAttribPointer are functions which are used by us to inform OpenGL of how our vertex structure Vertex is constructed.

	delete[] aoVertices;
	delete[] auiIndices;
	delete[] perlinData;
}

void ProceduralGeneration::Render(FlyCamera* _camera)
{
	shader.Use();
	unsigned int projectionViewUniform = glGetUniformLocation(shader.Program, "ProjectionView");
	glUniformMatrix4fv(projectionViewUniform, 1, false, glm::value_ptr(_camera->getProjectionView()));

	unsigned int timeUniform = glGetUniformLocation(shader.Program, "time");
	glUniform1f(timeUniform, m_deltaTime);

	unsigned int heightUniform = glGetUniformLocation(shader.Program, "heightScale");
	glUniform1f(heightUniform, height);

	glBindVertexArray(m_VAO);
	unsigned int indexCount = (m_rows - 1) * (m_cols - 1) * 6;

	//rock
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture);

	//sand
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_perlin_texture2);


	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBindVertexArray(0);
}

int ProceduralGeneration::Shutdown()
{
	return 0;
}
