#include "Program.h"



Program::Program(int _width, int _height, const char* name) : Application(_width, _height, name)
{

	Mountain = new ProceduralGeneration();
	Water = new RenderingGeometry();
	Snow = new ParticleEmitter();
	Wons = new NonThreadedParticle();
	//loader = new FBXLoader("soulspear",vec3(30,15,25));
	//cube = new FBXLoader("medic", vec3(15, 8, 6));
	
	boundingSphere = new BoundingSphere();

	boundingSphere->centre = vec3(0, 0, 0);
	boundingSphere->radius = 5;
	
	//Camera Setup
	camera = FlyCamera();
	camera.SetInputWindow(m_window);
	camera.setSpeed(17.5f);
	camera.setRotSpeed(0.15f);
	camera.setPostion(vec3(100, 0, 100));

}


Program::~Program()
{
}

void Program::Startup()
{
	//loader->Startup();
	//cube->Startup();
	Mountain->Startup();
	Water->Startup();
	Snow->Startup(800, 200, 0.1f, 5.0f, 1, 5, 0.1f, 1, glm::vec4(0, 0, 0, 1), glm::vec4(0, 0, 0, 0));
	Wons->Startup(800, 200, 0.1f, 5.0f, 1, 5, 0.1f, 1, glm::vec4(1, 0, 0, 1), glm::vec4(1, 0, 0, 1));
}

void Program::Update(float deltaTime)
{
	deltaTime = m_deltaTime;
	camera.Update(deltaTime);
	Snow->Update(deltaTime, camera.GetTransform());
	Wons->Update(deltaTime, camera.GetTransform());
	glClearColor(m_clearColour.r, m_clearColour.g, m_clearColour.b, 1);
	ImGui::ColorEdit3("clear color", glm::value_ptr(m_clearColour));
}

void Program::Render()
{
	if (camera.Culling(boundingSphere))
	{
	//loader->Render(&camera);
	}
	//cube->Render(&camera);
	Mountain->Render(&camera);		
	Snow->Draw(&camera);
	Wons->Render(&camera);
	Water->Render(&camera);
	ImGui::Render();
}

int Program::Shutdown()
{
	//loader->Shutdown();
	//cube->Shutdown();
	return 0;
}
