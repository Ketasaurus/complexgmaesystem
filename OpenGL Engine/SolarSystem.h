#pragma once
#include "Application.h"
#include "FlyCamera.h"



class SolarSystem : public Application
{
public:
	SolarSystem(int _width, int _height, const char* name);
	~SolarSystem();

	void Render();

	void Start();

	void Update(float deltaTime);

protected:

	glm::mat4 sunRot;
	glm::mat4 planetRot;
	glm::mat4 moonRot;
	
	FlyCamera camera;

	float elapsed;
};

