#include "Camera.h"



Camera::Camera()
{
	setPerspective(glm::pi<float>() * 0.25f, 16 / 9.f, 0.1f, 1000.f);
	setLookAt(vec3(10, 10, 10), vec3(0), vec3(0, 1, 0));
}


Camera::~Camera()
{
}

void Camera::Update(float deltaTime)
{
}

void Camera::setPerspective(float FoV, float aspectRatio, float w_near, float w_far)
{
	projectionTransform = (glm::perspective(FoV, aspectRatio, w_near, w_far));
	updateProjectionView();
}

void Camera::setLookAt(vec3 from, vec3 to, vec3 up)
{
	viewTranform = (glm::lookAt(from, to , up));
	worldTransform = glm::inverse(viewTranform);
	updateProjectionView();
}

void Camera::setPostion(vec3 position)
{
	worldTransform[3] = vec4(position, 1);
	viewTranform = glm::inverse(worldTransform);
	updateProjectionView();
}

void Camera::setTransform(glm::mat4 transform)
{
	worldTransform = transform;
	viewTranform = glm::inverse(worldTransform);
	updateProjectionView();
}

glm::mat4 Camera::GetTransform()
{
	return worldTransform;
}

glm::vec3 Camera::getPosition()
{
	return glm::vec3(worldTransform[3].x, worldTransform[3].y, worldTransform[3].z);
}

mat4 Camera::getWorldTransform()
{

	return worldTransform;
}

mat4 Camera::getView()
{
	return viewTranform;
}

mat4 Camera::getProjection()
{
	return projectionTransform;
}

mat4 Camera::getProjectionView()
{
	return projectionViewTransform;
}

void Camera::updateProjectionView()
{
	projectionViewTransform = projectionTransform * viewTranform;
}

void Camera::RotateX(const float angle)
{
	worldTransform = glm::rotate(angle, glm::vec3(1, 0, 0));
}

void Camera::RotateY(const float angle)
{
	worldTransform = glm::rotate(angle, glm::vec3(0, 1, 0));
}

void Camera::RotateZ(const float angle)
{
	worldTransform = glm::rotate(angle, glm::vec3(0, 0, 1));
}


