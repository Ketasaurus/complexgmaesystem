#pragma once
#include <gl_core_4_4.h>
#include <glfw\include\GLFW\glfw3.h>
#include <cstdio>
#include "Gizmos.h"
#include <glm\glm\glm.hpp>
#include <glm\glm\ext.hpp>

using glm::vec3;
using glm::vec4;
using glm::mat4;


class Camera
{
public:

	Camera();
	~Camera();
	virtual void Update(float deltaTime);
	void setPerspective(float FoV, float aspectRatio, float w_near, float w_far);
	void setLookAt(vec3 from, vec3 to, vec3 up);
	void setPostion(vec3 position);
	void setTransform(glm::mat4 transform);
	glm::mat4 GetTransform();
	glm::vec3 getPosition();
	mat4 getWorldTransform();
	mat4 getView();
	mat4 getProjection();
	mat4 getProjectionView();

	void RotateX(const float angle);
	void RotateY(const float angle);
	void RotateZ(const float angle);

protected:

	void updateProjectionView();

	mat4 worldTransform;
	mat4 viewTranform;
	mat4 projectionTransform;
	mat4 projectionViewTransform;

};

