#pragma once
#include "Application.h"
#include "ProceduralGeneration.h"
#include "ParticleEmitter.h"
#include "RenderingGeometry.h"
#include "FlyCamera.h"
#include "FBXLoader.h"
#include "BoundingSphere.h"
#include "NonThreadedParticle.h"

class Program :
	public Application
{
public:
	Program(int _width, int _height, const char* name);
	~Program();

	void Startup() override;

	void Update(float deltaTime) override;

	void Render() override;

	int Shutdown() override;

protected:
	BoundingSphere* boundingSphere;
	ProceduralGeneration* Mountain;
	RenderingGeometry* Water;
	ParticleEmitter* Snow;
	NonThreadedParticle* Wons;
	FBXLoader* loader;
	FBXLoader* cube;
	FlyCamera camera;

	glm::vec4 m_clearColour = glm::vec4(1,1,1,1);
};

