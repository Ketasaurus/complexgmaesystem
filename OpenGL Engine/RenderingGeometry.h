#pragma once
#include "Application.h"
#include "FlyCamera.h"
#include "Shader.h"
#include "ParticleEmitter.h"
#include <stb-master\stb_image.h>
#include <glm\glm\glm.hpp>
class RenderingGeometry : public Application
{
public:

	//structure that we can use to populate with vertex information.
	struct Vertex
	{
		vec4 position;
		vec4 colour;
	};

	RenderingGeometry();

	RenderingGeometry(int _width, int _height, const char* name);

	~RenderingGeometry();
	//generating a grid of vertex points into our code
	void generateGrid(unsigned int rows, unsigned int cols);

	void Startup();

	void Update(float deltaTime);

	int Shutdown();

	void Render(FlyCamera* _camera);
protected:

	Shader shader;
	ParticleEmitter* emitter;
	GLuint m_rows, m_cols;
	float height = 12;
	unsigned int m_perlin_texture2;
};

