#include <gl_core_4_4.h>
#include <glfw\include\GLFW\glfw3.h>
#include <cstdio>
#include "Gizmos.h"
#include <glm\glm\glm.hpp>
#include <glm\glm\ext.hpp>
#include "SolarSystem.h"
#include "RenderingGeometry.h"
#include "Application.h"
#include "ParticleEmitter.h"
#include "FBXLoader.h"
#include "ProceduralGeneration.h"
#include "Program.h"
#include <iostream>


using glm::vec3;
using glm::vec4;
using glm::mat4;


int main()
{
	Program fbxtest(1280, 720, "OpenGL Engine");
	fbxtest.Run();
}