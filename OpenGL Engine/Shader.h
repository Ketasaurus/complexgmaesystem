#pragma once
#include "Application.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader
{
public:
	GLuint Program;
	
	Shader() {};
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	~Shader();

	void Use();
	unsigned int loadShader(unsigned int type, const char* path);


};

