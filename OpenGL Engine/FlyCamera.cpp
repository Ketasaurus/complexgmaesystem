#include "FlyCamera.h"

#include <iostream>

FlyCamera::FlyCamera()
{
	viewButtonPressed = false;
}

FlyCamera::FlyCamera(float flySpeed, float rotSpeed) : Camera(), flySpeed(flySpeed), rotationSpeed(rotSpeed), m_Window(nullptr)
{

}


FlyCamera::~FlyCamera()
{

}

void FlyCamera::Update(float deltaTime)
{
	assert(m_Window != nullptr);

	HandleKeyboardInput(deltaTime);
	HandleMouseInput(deltaTime);
}

void FlyCamera::setSpeed(float speed)
{
	flySpeed = speed;
}

float FlyCamera::getSpeed(float speed)
{
	return flySpeed;
}

float FlyCamera::getRotSpeed(float rotSpeed) 
{
	return rotationSpeed;
}

void FlyCamera::setRotSpeed(float rotSpeed)
{
	rotationSpeed = rotSpeed;
}

bool FlyCamera::Culling(BoundingSphere * boundingSphere)
{
	vec4 planes[6];
	 getFrustumPlanes(getProjectionView(), planes);
	for (int i = 0; i < 6; i++)
	{
		float d = glm::dot(vec3(planes[i]), boundingSphere->centre) +
		planes[i].w;
		if (d < -boundingSphere->radius)
		{
			
			return false;
			break;
		}

	}

	
	return true;
}

void FlyCamera::getFrustumPlanes(const glm::mat4 & transform, glm::vec4 * planes)
{
	// right side
	planes[0] = vec4(transform[0][3] - transform[0][0],
		transform[1][3] - transform[1][0],
		transform[2][3] - transform[2][0],
		transform[3][3] - transform[3][0]);
	// left side
	planes[1] = vec4(transform[0][3] + transform[0][0],
		transform[1][3] + transform[1][0],
		transform[2][3] + transform[2][0],
		transform[3][3] + transform[3][0]);
	// top
	planes[2] = vec4(transform[0][3] - transform[0][1],
		transform[1][3] - transform[1][1],
		transform[2][3] - transform[2][1],
		transform[3][3] - transform[3][1]);
	// bottom
	planes[3] = vec4(transform[0][3] + transform[0][1],
		transform[1][3] + transform[1][1],
		transform[2][3] + transform[2][1],
		transform[3][3] + transform[3][1]);
	// far
	planes[4] = vec4(transform[0][3] - transform[0][2],
		transform[1][3] - transform[1][2],
		transform[2][3] - transform[2][2],
		transform[3][3] - transform[3][2]);
	// near
	planes[5] = vec4(transform[0][3] + transform[0][2],
		transform[1][3] + transform[1][2],
		transform[2][3] + transform[2][2],
		transform[3][3] + transform[3][2]);
	// plane normalisation, based on length of normal
	for (int i = 0; i < 6; i++) {
		float d = glm::length(vec3(planes[i]));
		planes[i] /= d;
	}

}

void FlyCamera::HandleKeyboardInput(float deltaTime)
{
	glm::mat4 transform = getWorldTransform();

	glm::vec3 m_Right = glm::vec3(transform[0].x, transform[0].y, transform[0].z);
	glm::vec3 m_Up = glm::vec3(transform[1].x, transform[0].y, transform[1].z);
	glm::vec3 m_Foward = glm::vec3(transform[2].x, transform[2].y, transform[2].z);

	glm::vec3 moveDirection(0.0f);
	//Foward and backwards camera movement
	if (glfwGetKey(m_Window, GLFW_KEY_W) == GLFW_PRESS)
	{
		moveDirection -= m_Foward;
	}
	if (glfwGetKey(m_Window, GLFW_KEY_S) == GLFW_PRESS)
	{
		moveDirection += m_Foward;
	}
	//Left and right camera movement
	if (glfwGetKey(m_Window, GLFW_KEY_A) == GLFW_PRESS)
	{
		moveDirection -= m_Right;
	}
	if (glfwGetKey(m_Window, GLFW_KEY_D) == GLFW_PRESS)
	{
		moveDirection += m_Right;
	}
	//Up and down  camera movement
	if (glfwGetKey(m_Window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		moveDirection += m_Up;
	}
	if (glfwGetKey(m_Window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	{
		moveDirection -= m_Up;
	}

	float fLength = glm::length(moveDirection);
	if (fLength > 0.01f)
	{
		moveDirection = ((float)deltaTime * flySpeed) * glm::normalize(moveDirection);


		setPostion(getPosition() + moveDirection);
	}
	
}

void FlyCamera::HandleMouseInput(float deltaTime)
{
	if (glfwGetMouseButton(m_Window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
	{
		if (viewButtonPressed == false)
		{
			int width, height;
			glfwGetFramebufferSize(m_Window, &width, &height);

			cursorX = width / 2.0;
			cursorY = height / 2.0;

			glfwSetCursorPos(m_Window, width / 2, height / 2);

			viewButtonPressed = true;
		}
		else
		{
			double mouseX, mouseY;
			glfwGetCursorPos(m_Window, &mouseX, &mouseY);

			double xOffset = mouseX - cursorX;
			double yOffset = mouseY - cursorY;


			//std::cout << deltaTime << std::endl;

			CalculateRotation(deltaTime, xOffset, yOffset);
		}

		int width, height;
		glfwGetFramebufferSize(m_Window, &width, &height);
		glfwSetCursorPos(m_Window, width / 2, height / 2);
	}
	else
	{
		viewButtonPressed = false;
	}
}

void FlyCamera::CalculateRotation(float deltaTime, double xOffset, double yOffset)
{
	if (xOffset == 0 && yOffset == 0) return;
	
	if (xOffset != 0.0)
	{
		//RotateY(rotationSpeed * deltaTime * -xOffset);
		//RotateX(rotationSpeed * deltaTime * -yOffset);
		
		glm::mat4 rot = glm::rotate((float)(rotationSpeed * deltaTime * -xOffset), glm::vec3(0, 1, 0));

		setTransform(GetTransform() * rot);
	}

	if (yOffset != 0.0)
	{
		glm::mat4 rot = glm::rotate((float)(rotationSpeed * deltaTime * -yOffset), glm::vec3(1, 0, 0));
		setTransform(GetTransform() * rot);
	}

	glm::mat4 oldTransform = GetTransform();
	
	glm::mat4 transform(1);
	
	glm::vec3 worldUp = glm::vec3(0, 1, 0);
	
	//Right
	glm::vec3 oldFoward = glm::vec3(oldTransform[2].x, oldTransform[2].y, oldTransform[2].z);
	transform[0] = glm::normalize(glm::vec4(glm::cross(worldUp, oldFoward), 0));
	
	//Up
	glm::vec3 newRight = glm::vec3(transform[0].x, transform[0].y, transform[0].z);
	transform[1] = glm::normalize(glm::vec4(glm::cross(oldFoward, newRight), 0));
	
	//Foward
	transform[2] = glm::normalize(oldTransform[2]);
	
	//Position
	transform[3] = oldTransform[3];
	
	setTransform(transform);

}
