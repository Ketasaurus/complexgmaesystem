#pragma once
#include "Application.h"
#include "FBXFile.h"
#include "Shader.h"
#include "FlyCamera.h"
#include <stb-master\stb_image.h>

class FBXLoader :
	public Application
{
public:

	FBXLoader(std::string _name, vec3 position);
	FBXLoader(int _width, int _height, const char* name, std::string _name);
	~FBXLoader();

	void createOpenGLBuffers(FBXFile* fbx);

	void cleanOpenGLBuffers(FBXFile* fbx);

	int Shutdown();

	void Initialise();

	void Render(FlyCamera* _camera);

	void Update(float deltaTime);

	void Startup();

	void DrawMesh();

	glm::mat4 Transform;
protected:
	FlyCamera camera;
	FBXFile* m_fbx;
	Shader shader;
//////////////////////////
	std::string fileLocation = ("./Models/");
	std::string modelName;
	std::string finalModel;
	std::string FBX = (".fbx");
	
	unsigned int m_texture, m_normalmap;
	int imageWidth = 0;
	int imageHeight = 0;
	int imageFormat = 0;
	unsigned int uniformlightDir;
	unsigned int uniformLightColour;
	unsigned int uniformCameraPos;
	unsigned int uniformSpecPow;
};

