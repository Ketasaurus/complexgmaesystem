#include "RenderingGeometry.h"



RenderingGeometry::RenderingGeometry()
{
	shader = Shader("./Shaders/Vertex/default.vert", "./Shaders/Fragment/default.frag");
	shader.Use();
}

RenderingGeometry::RenderingGeometry(int _width, int _height, const char* name) : Application(_width, _height, name) 
{
	shader = Shader("./Shaders/Vertex/default.vert", "./Shaders/Fragment/default.frag");
	shader.Use();
}


RenderingGeometry::~RenderingGeometry()
{
}

//accepts some number of rows and columns as arguments and then generates and deletes a grid,
void RenderingGeometry::generateGrid(unsigned int rows, unsigned int cols)
{
	//generates new vertex using rows and cols as viriables
	Vertex* aoVertices = new Vertex[rows * cols];


			// defining index count based off quad count (2 triangles per quad)
			unsigned int* auiIndices = new  unsigned int[(rows - 1)* (cols - 1) * 6];

			for (unsigned int r = 0; r < rows; ++r) {

				for (unsigned int c = 0; c < cols; ++c) {

					// defining index count based off quad count (2 triangles per quad
				
					aoVertices[r * cols + c].position = vec4((float)c, 8, (float)r, 1);

					glEnable(GL_DEPTH_TEST);
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				}
			}
			m_rows = rows;
			m_cols = cols;

			unsigned int index = 0;
			for (unsigned int r = 0; r < (rows - 1); ++r) {

				for (unsigned int c = 0; c < (cols - 1); ++c) {

					// triangle 1
					auiIndices[index++] = r * cols + c;
					auiIndices[index++] = (r + 1) * cols + c;
					auiIndices[index++] = (r + 1) * cols + (c + 1);
					// triangle 2
					auiIndices[index++] = r * cols + c;
					auiIndices[index++] = (r + 1) * cols + (c + 1);
					auiIndices[index++] = r * cols + (c + 1);
					//The above code segment creates an index array, as we are drawing our grid as a series of quads we have generated the index positions for each triangle in this grid.
				}
			}

			// create and bind buffers to a vertex array object
			glGenBuffers(1, &m_VBO);
			glGenBuffers(1, &m_IBO);

			glGenVertexArrays(1, &m_VAO);
			glBindVertexArray(m_VAO);

			glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);

			glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(Vertex), aoVertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4)));


			glBindVertexArray(0);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			//This code segment will create and bind our VBO and fill it with the vertex data for our grid. 
			// glEnableVertexAttribArray and glVertexAttribPointer are functions which are used by us to inform OpenGL of how our vertex structure Vertex is constructed.
		
	
	
	delete[] aoVertices;
}

void RenderingGeometry::Startup()
{
	generateGrid(100, 100);
}

void RenderingGeometry::Update(float deltaTime)
{

}

int RenderingGeometry::Shutdown()
{
	return 0;
}

void RenderingGeometry::Render(FlyCamera* _camera)
{
	shader.Use();
	unsigned int projectionViewUniform = glGetUniformLocation(shader.Program, "ProjectionView");
	glUniformMatrix4fv(projectionViewUniform, 1, false, glm::value_ptr(_camera->getProjectionView()));

	unsigned int timeUniform = glGetUniformLocation(shader.Program, "time");
	glUniform1f(timeUniform, m_deltaTime);

	unsigned int heightUniform = glGetUniformLocation(shader.Program, "heightScale");
	glUniform1f(heightUniform, height );

	unsigned int transparencyUniform = glGetUniformLocation(shader.Program, "transparency");
	glUniform1f(transparencyUniform, 0.5f);

	glBindVertexArray(m_VAO);
	unsigned int indexCount = (m_rows - 1) * (m_cols - 1) * 6;




	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBindVertexArray(0);
}
