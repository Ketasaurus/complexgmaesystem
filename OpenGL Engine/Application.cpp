#include "Application.h"


Application::Application()
	:m_previousTime(0.0f), m_deltaTime(0.0f)
{
}

Application::Application(int _width, int _height, const char* name, GLFWmonitor * _monitor, GLFWwindow* _share)
	: m_previousTime(0.0f), m_deltaTime(0.0f)
{
	initGLFW(_width, _height, name);
	initGL();
	ImGui_ImplGlfwGL3_Init(m_window, true);

	ImGuiIO& io = ImGui::GetIO();
	io.DisplaySize.x = m_uiScreenWidth;
	io.DisplaySize.y = m_uiScreenHeight;
}


Application::~Application()
{

}

void Application::Run()
{
	Startup();

	while (glfwWindowShouldClose(m_window) == false && glfwGetKey(m_window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		ImGui_ImplGlfwGL3_NewFrame();
		float currentTime = (float)glfwGetTime();
		m_deltaTime = currentTime - m_previousTime;
		m_previousTime = currentTime;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		Update(m_deltaTime);

		Render();

		glfwSwapBuffers(m_window);
		glfwPollEvents();
	}

	Shutdown();
}

void Application::Update(float deltaTime)
{
}

void Application::Render()
{
}

void Application::Startup()
{
	auto major = ogl_GetMajorVersion();
	auto minor = ogl_GetMinorVersion();
	printf("GL: %i.%i\n", major, minor);
}

int Application::Shutdown()
{
	shutdownGLFW();
	ImGui_ImplGlfwGL3_Shutdown();
	Gizmos::destroy();
	return 0;
}

int Application::initGLFW(int width, int height, const char* name)
{
	if (glfwInit() == false)
		return -1;

	m_window = glfwCreateWindow(width, height, name, nullptr, nullptr);

	if (m_window == nullptr)
	{
		glfwTerminate();
		return -2;
	}

	
	glfwMakeContextCurrent(m_window);

	return 0;
}

int Application::initGL()
{
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED) {
		glfwDestroyWindow(m_window);
		glfwTerminate();
		return -3;
	}

	return 0;
}

void Application::setupProjection(mat4 a_projection)
{
	m_projection = a_projection;
}

void Application::setupView(mat4 a_view)
{
	m_view = a_view;
}


void Application::shutdownGLFW()
{
	glfwDestroyWindow(m_window);
	glfwTerminate();
}

float Application::GetdeltaTime()
{
	float currentTime = (float)glfwGetTime();
	m_deltaTime = currentTime - m_previousTime;
	m_previousTime = currentTime;
	return m_deltaTime;
}

void Application::Initialise()
{	

}




