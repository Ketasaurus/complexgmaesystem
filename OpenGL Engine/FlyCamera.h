#pragma once
#include "Camera.h"
#include "BoundingSphere.h"

class FlyCamera : public Camera
{
public:

	FlyCamera();

	FlyCamera(float flySpeed, float rotSpeed);

	~FlyCamera();

	virtual void Update(float deltaTime) override;

	void setSpeed(float speed);
	float getSpeed(float speed);

	float getRotSpeed(float rotSpeed);
	void setRotSpeed(float rotSpeed);

	void SetInputWindow(GLFWwindow* pWindow)
	{
		m_Window = pWindow;
	}

	bool Culling(BoundingSphere* boundingSphere);

	void getFrustumPlanes(const glm::mat4& transform, glm::vec4* planes);

protected:

	void HandleKeyboardInput(float deltaTime);
	void HandleMouseInput(float deltaTime);

	void CalculateRotation(float deltaTime, double xOffset, double yOffset);

	GLFWwindow* m_Window;
	float flySpeed;
	float rotationSpeed;
	bool viewButtonPressed;
	double cursorX, cursorY;
	vec3 up;

};

