#include "FBXLoader.h"



FBXLoader::FBXLoader(std::string _name, vec3 position )
{
	shader = Shader("./Shaders/Vertex/fbx.vert", "./Shaders/Fragment/fbx.frag");
	
	Initialise();
	finalModel = fileLocation + _name + FBX;
	m_fbx = new FBXFile();
	m_fbx->load(finalModel.c_str());
	createOpenGLBuffers(m_fbx);
	Transform = glm::translate(Transform, position);
}

FBXLoader::FBXLoader(int _width, int _height, const char * name, std::string _name) : Application(_width, _height, name)
{
	shader = Shader("./Shaders/Vertex/fbx.vert", "./Shaders/Fragment/fbx.frag");
	Initialise();
	finalModel = fileLocation + _name + FBX;
	m_fbx = new FBXFile();
	m_fbx->load(finalModel.c_str());
	createOpenGLBuffers(m_fbx);

}


FBXLoader::~FBXLoader()
{
}

void FBXLoader::createOpenGLBuffers(FBXFile * fbx)
{
	fbx->initialiseOpenGLTextures();
	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);

		unsigned int* glData = new unsigned int[3];

		glGenVertexArrays(1, &glData[0]);
		glBindVertexArray(glData[0]);
		glGenBuffers(1, &glData[1]);
		glGenBuffers(1, &glData[2]);
		glBindBuffer(GL_ARRAY_BUFFER, glData[1]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glData[2]);
		glBufferData(GL_ARRAY_BUFFER, mesh->m_vertices.size() * sizeof(FBXVertex), mesh->m_vertices.data(), GL_STATIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->m_indices.size() * sizeof(unsigned int), mesh->m_indices.data(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(0); // position
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), 0);
		glEnableVertexAttribArray(1); // normal
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char*)0) + FBXVertex::NormalOffset);
		glEnableVertexAttribArray(2); // normal
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::TexCoord1Offset);
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		mesh->m_userData = glData;
	}
}

void FBXLoader::cleanOpenGLBuffers(FBXFile * fbx)
{
	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);
		unsigned int* glData = (unsigned int*)mesh->m_userData;
		glDeleteVertexArrays(1, &glData[0]);
		glDeleteBuffers(1, &glData[1]);
		glDeleteBuffers(1, &glData[2]);
		delete[] glData;
	}
}


void FBXLoader::Startup()
{
	Gizmos::create();


	//Setup Data For Texture
	unsigned char* data = stbi_load("./Textures/rock_diffuse.tga", &imageWidth, &imageHeight, &imageFormat, STBI_rgb_alpha);

	//Load diffuse map
	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth, imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//Free the data from texture
	stbi_image_free(data);


	unsigned char* Data = stbi_load("./Textures/rock_normal.tga", &imageWidth, &imageHeight, &imageFormat, STBI_rgb_alpha);
	//Load normal map
	glGenTextures(1, &m_normalmap);
	glBindTexture(GL_TEXTURE_2D, m_normalmap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth, imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//Free the data from texture
	stbi_image_free(Data);


}

void FBXLoader::DrawMesh()
{
	for (unsigned int i = 0; i < m_fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = m_fbx->getMeshByIndex(i);
		unsigned int* glData = (unsigned int*)mesh->m_userData;
		glBindVertexArray(glData[0]);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->m_material->textures[FBXMaterial::DiffuseTexture]->handle);

		if (mesh->m_material->textures[FBXMaterial::NormalTexture])
		{
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, mesh->m_material->textures[FBXMaterial::NormalTexture]->handle);
		}
		else
		{
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		glDrawElements(GL_TRIANGLES, (unsigned int)mesh->m_indices.size(), GL_UNSIGNED_INT, 0);
	}
}

void FBXLoader::Render(FlyCamera* _camera)
{
		// use our texture program
		glUseProgram(shader.Program);

		// bind the camera
		unsigned int projectionViewUniform = glGetUniformLocation(shader.Program, "ProjectionView");
		glUniformMatrix4fv(projectionViewUniform, 1, GL_FALSE, glm::value_ptr(_camera->getProjectionView()));

		

		// set texture slots
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_texture);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_normalmap);

		// tell the shader where it is
		projectionViewUniform = glGetUniformLocation(shader.Program, "Texture");
		glUniform1i(projectionViewUniform, 0);
		projectionViewUniform = glGetUniformLocation(shader.Program, "normal");
		glUniform1i(projectionViewUniform, 1);

		glm::vec3 lightDir(0, 0.75, 0);
		glm::vec3 lightColour(1, 1, 1);
		float specPower = 200;

		mat4 camTrans = camera.getWorldTransform();
		vec3 camPos = glm::vec3(camTrans[3].x, camTrans[3].y, camTrans[3].z);
		
		// bind the light
		uniformlightDir = glGetUniformLocation(shader.Program, "LightDir");
		glUniform3fv(uniformlightDir, 1, glm::value_ptr(lightDir));

		uniformLightColour = glGetUniformLocation(shader.Program, "LightColour");
		glUniform3fv(uniformLightColour, 1, glm::value_ptr(lightColour));

		uniformCameraPos = glGetUniformLocation(shader.Program, "CameraPos");
		glUniform3fv(uniformCameraPos, 1, glm::value_ptr(camPos));

		uniformSpecPow = glGetUniformLocation(shader.Program, "SpecPow");
		glUniform1f(uniformSpecPow, specPower);

		int loc = glGetUniformLocation(shader.Program, "projectionView");
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(_camera->getProjectionView()));

		 loc = glGetUniformLocation(shader.Program, "Transform");
		glUniformMatrix4fv(loc, 1, GL_FALSE, &Transform[0][0]);

		DrawMesh();

}

void FBXLoader::Update(float deltaTime)
{


}


int FBXLoader::Shutdown()
{
	//Clean up excess data
	cleanOpenGLBuffers(m_fbx);
	Gizmos::destroy();
	glDeleteProgram(shader.Program);
	return 0;
}

void FBXLoader::Initialise()
{
	
	glEnable(GL_DEPTH_TEST);

	glCullFace(GL_FRONT_AND_BACK);
}
