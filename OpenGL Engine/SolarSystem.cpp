#include "SolarSystem.h"



SolarSystem::SolarSystem(int _width, int _height, const char* name)
	: Application(_width, _height, name)
{
	camera = FlyCamera();
	camera.setPerspective(glm::pi<float>() * 0.25f, 16 / 9.f, 0.1f, 1000.f);
	camera.setLookAt(vec3(10, 10, 10), vec3(0), vec3(0, 1, 0));
	camera.setSpeed(1.5f);
	camera.setRotSpeed(0.20f);
}


SolarSystem::~SolarSystem()
{

}

void SolarSystem::Render()
{
	++elapsed;

	sunRot = glm::rotate(sunRot, glm::radians(.2f), vec3(0, 1, 0));

	planetRot = glm::rotate(sunRot, glm::radians(5.0f), vec3(0, 1, 0));
	planetRot = glm::translate(planetRot, vec3(6, 0, 0));

	moonRot = glm::rotate(planetRot, glm::radians(2.0f * elapsed), vec3(0, 1, 0));
	moonRot = glm::translate(moonRot, vec3(2, 0, 0));


	//Gizmos::addSphere(vec3(0, 0, 0), 2, 50, 50, vec4(255.0f, 125.0f, 0.0f, 1), &sunRot);
	
	//Gizmos::addSphere(planetRot[3].xyz, 0.5, 50, 50, vec4(0.0f, 0.0f, 200.0f, 1), &planetRot);

	//Gizmos::addSphere(moonRot[3].xyz, 0.25, 25, 25, vec4(0.25f, 0.25f, 0.25f, 1), &moonRot);

	//Gizmos::draw(camera.getProjectionView());
}

void SolarSystem::Start()
{
	camera.SetInputWindow(m_window);
	moonRot = glm::translate(moonRot, vec3(2, 0, 0));
	planetRot = glm::translate(planetRot, vec3(6, 0, 0));
	elapsed = 0;
}

void SolarSystem::Update(float deltaTime)
{
	camera.Update(deltaTime);
}


