#pragma once
#include "Application.h"
#include "FlyCamera.h"
#include "Shader.h"
#include "ParticleEmitter.h"
#include <stb-master\stb_image.h>
#include <glm\glm\glm.hpp>

class ProceduralGeneration : public Application
{
public:
	struct Vertex
	{
		vec4 position;
		vec4 colour;
		vec2 texCoord;
	};

	ProceduralGeneration(int _width, int _height, const char* name);
	ProceduralGeneration();
	~ProceduralGeneration();

	void Startup();

	void Update(float deltaTime);

	void generateGrid(unsigned int rows, unsigned int cols);

	void Render(FlyCamera* _Camera);

	int Shutdown();

	
	float frequency = 1.5f;
	float amplitude = 20;

protected: 

	mat4 m_Transform;
	Shader shader;
	ParticleEmitter* emitter;
	GLuint m_rows, m_cols;
	float height = 12;
	unsigned int m_perlin_texture;
	unsigned int m_perlin_texture2;
	unsigned int m_perlin_texture3;
};

