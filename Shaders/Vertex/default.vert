#version 410

layout (location=0) in vec4 Position;

layout (location=1) in vec4 Colour;

layout (location=2) in vec2 texCoord;

out vec4 vColour;
out vec4 vPosition;

out vec2 vTexCoord;

uniform mat4 ProjectionView;

uniform float time;

uniform float heightScale;

out float vHeightScale;

void main()
{
vHeightScale = heightScale;
  vColour = Colour;
  
  vec4 P = Position;
  
  vTexCoord = texCoord;
  
 // P.y += sin( time + Position.x ) * heightScale;
  vPosition = P;  
  gl_Position = ProjectionView * P;
}

