#version 410
in vec4 vPosition;

in vec4 vColour;

in vec2 vTexCoord;

out vec4 FragColour;

uniform sampler2D snow;
uniform sampler2D water;

uniform float transparency = 1;

in float vHeightScale;


void main()
{
vec4 snowC = texture(snow,vTexCoord);
vec4 waterC = texture(water,vTexCoord);

FragColour = mix(waterC, snowC, vPosition.y / vHeightScale);
FragColour.a = transparency;
}