#version 410

in vec2 vTexCoord;

in vec3 vNormal; 

in vec3 vTangent; 

in vec3 vBiTangent; 

in vec4 vPosition; 

out vec4 FragColor; 

uniform sampler2D Texture;

uniform sampler2D normal; 

uniform vec3 LightDir; 

uniform vec3 LightColour; 

uniform vec3 CameraPos; 

uniform float SpecPow; 

void main() 
{ 
  mat3 TBN = mat3(normalize( vTangent ),normalize( vBiTangent ),normalize( vNormal )); 
  vec3 N = texture(normal,vTexCoord).xyz * 2 - 1; 
  float d = max(0,dot(normalize(vNormal.xyz),LightDir ) ); 
  vec3 E = normalize( CameraPos - vPosition.xyz );
  vec3 R = reflect( -LightDir,vNormal.xyz ); 
  float s = max( 0, dot( E, R ) ); 
  s = pow( s, SpecPow ); 
  FragColor = vec4( texture(Texture, vTexCoord).xyz * LightColour * d + LightColour * s, 1);
}